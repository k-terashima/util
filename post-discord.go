package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	ds "github.com/bwmarrin/discordgo"
)

func PostDiscord(w ds.Webhook, content string) error {
	// リクエスト準備
	var (
		b      ds.WebhookParams
		client http.Client
	)

	url := ds.EndpointWebhookToken(w.ID, w.Token)
	b.Username = w.Name
	b.Content = content
	j, _ := json.Marshal(b)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(j))
	req.Header.Set("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

	// 投稿未遂チェック
	if res.StatusCode != 200 {
		return fmt.Errorf(res.Status)
	}

	return nil
}
